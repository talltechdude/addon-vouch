#!/usr/bin/with-contenv bashio
# ==============================================================================
# TallTechDude Hass.io Add-ons: Vouch Proxy
# Runs the Vouch Proxy Server
# ==============================================================================

if ! bashio::fs.directory_exists "/opt/vouch/config"; then
    mkdir -p /data/config
    ln -s /data/config /opt/vouch/config
fi

if ! bashio::fs.file_exists "/data/config/session.key"; then
    export SESSION_KEY=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 44 | head -n 1)
    echo $SESSION_KEY > /data/config/session.key
else
    export SESSION_KEY=$(</data/config/session.key)
fi

export LOG_LEVEL=$(bashio::config 'log_level')
export DOMAIN=$(bashio::config 'domain')
export JWT_MAX_AGE=$(bashio::config 'jwt_max_age')
export VOUCH_URL=$(bashio::config 'vouch_url')
export HOMEASSISTANT_URL=$(bashio::config 'homeassistant_url')

envsubst '$LOG_LEVEL $DOMAIN $JWT_MAX_AGE $VOUCH_URL $HOMEASSISTANT_URL $SESSION_KEY' < "/opt/config.yml" > "/data/config/config.yml" 

